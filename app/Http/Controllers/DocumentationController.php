<?php

namespace App\Http\Controllers;

use Illuminate\Http\Response;
use \Facades\App\Documentation;

class DocumentationController extends Controller
{
    protected $docs;

    /**
     * Display the specified resource.
     *
     * @param  string  $version
     * @param  string  $page
     * @return \Illuminate\Http\Response
     */
    public function show($version, $page = '')
    {
        if (! $this->validVersion($version)) {
            return redirect('docs/' . DEFAULT_VERSION . '/' . $version);
        }

        try {
            return view('docs', [
                'content' => Documentation::get($version, $page)
            ]);
        } catch (\Exception $exception) {
            abort(
                Response::HTTP_NOT_FOUND,
                'The requested documentation was not found.'
            );
        }
    }

    public function validVersion($version)
    {
        return in_array($version, Documentation::versions());
    }
}
