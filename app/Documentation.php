<?php

namespace App;

use File;
use Parsedown;

class Documentation
{
    public function get($version, $page, $basePath = null)
    {
        if (File::exists(
            $page = $this->markdownPath($version, $page, $basePath)
        )) {
            return $this->replaceLinks(
                $version,
                (new Parsedown)->text(File::get($page)),
                $basePath
            );
        }

        throw new \Exception('The requested documentation page was not found.');
    }

    public function markdownPath($version, $page, $basePath = null)
    {
        $basePath = $basePath ?? resource_path();

        return $basePath . '/docs/' . $version . '/' . $page . '.md';
    }

    public static function versions()
    {
        return [1.0, 1.1];
    }

    public function replaceLinks($version, $content)
    {
        return str_replace('{{version}}', $version, $content);
    }
}
